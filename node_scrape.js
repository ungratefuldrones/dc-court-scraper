try {
    var Spooky = require('spooky');
} catch (e) {
    var Spooky = require('../lib/spooky');
}
//var winston = require('winston');
var fs = require('fs');
var csvWriter = require('csv-write-stream');
var FixedArray = require("fixed-array");
var log4js = require('log4js');

//var notifier = require('node-notifier');
//var nc = new notifier.NotificationCenter();



var writer = csvWriter();
//var writer = csv();

// get commandline args

global.year = process.argv[2];
global.the_type = process.argv[3];
global.start = process.argv[4] || 1;
global.end = process.argv[5] || 4000;


log4js.loadAppender('file');
log4js.configure({
  appenders: [
   // { type: 'console' },
    { type: 'file', filename: global.year + '-' + global.the_type + '.log', category: 'scrape' }
  ]
});

var logger = log4js.getLogger('scrape');

writer.pipe(fs.createWriteStream(global.year + '_' + global.the_type + '.csv'))

//var Spreadsheet = require('edit-google-spreadsheet');

var spooky = new Spooky({
    casper: {
        logLevel: 'error'
        ,verbose: false
        ,waitTimeout: 10000
        ,options: {
            onResourceRequested : function(C, requestData, request) {
                  if ((/https?:\/\/.+?\.css/gi).test(requestData['url']) || requestData['Content-Type'] == 'text/css') {
                    console.log('Skipping CSS file: ' + requestData['url']);
                    request.abort();
                    }
                  }
        },
        pageSettings: {
            loadImages:  false,        // do not load images
            loadPlugins: false         // do not load NPAPI plugins (Flash, Silverlight, ...)
        }    
        //,clientScripts: ['bower_components/jquery/dist/jquery.min.js']
        //}
    }
}, function(err) {

    // NODE CONTEXT

    spooky.start('https://www.dccourts.gov/cco/maincase.jsf');
    var year = global.year;
    var the_type = global.the_type;


    spooky.then([{
        year: year,
        the_type: the_type
        ,start: start
        ,end: end
    }, function() {
        this.page.paperSize = {
            width: '8.5in',
            height: '13in',
            orientation: 'portrait',
            border: '0.4in'
        };

        var i = start;
        this.repeat(end, function() {
          //console.log(i);
            this.options.waitTimeout = 20000;
            this.waitUntilVisible('form[name="appData:searchform"]', function() {
                window.count = i;

                // fill out form
                this.fill('form[name="appData:searchform"]', {
                    'appData:searchform:jspsearchpage:j_id_id14pc3': year,
                    'appData:searchform:jspsearchpage:j_id_id18pc3': i.toString(),
                    'appData:searchform:jspsearchpage:selectCaseType': the_type,
                }, false);
            });

            // click search
            this.thenClick('input[name="appData:searchform:jspsearchpage:submitSearch"]');

            // wait until results visible
            this.waitUntilVisible('form[name="appData:searchform"]', function() {
                if (this.exists('input[name="appData:resultsform:jspresultspage:dt1:j_id_id41pc5"]')) {
                    this.thenClick('input[name="appData:resultsform:jspresultspage:dt1:j_id_id41pc5"]');

                    this.then(function() {
                        this.waitUntilVisible('tbody[id="appData:detailsform:jspdetailspage:docketInfo:DocketsInfo:tbody_element"]', function() {

                            this.page.injectJs('bower_components/jquery/dist/jquery.min.js');
                            // gather information
                            this.evaluate(function() {
                            
                                try {
                                    window.person = this.document.querySelector('tbody[id="appData:detailsform:jspdetailspage:partyInfo:partiesInfo:tbody_element"] > tr > td').innerHTML;
                                } catch (e) {
                                    window.person = '';
                                }
                                try {
                                    window.number = this.document.querySelectorAll('div[id="appData:searchform:searchBlock"] > div > div')[1].innerHTML.replace('Case Search for:', '');
                                } catch (e) {
                                    window.number = '';
                                }
                                //window.number = this.document.querySelector('div.casesummaryheader').innerHTML.replace(/&nbsp;/g, ''); 
                                try {
                                    window.date = this.document.querySelectorAll('table > tbody > tr > td.summaryfieldvalue > span')[1].innerHTML;
                                } catch (e) {
                                    window.date = '';
                                }

                                var charges = jQuery('tr[class^="TableRow"]:contains("Charge Filed")');
                                //var updates = jQuery('tr[class^="TableRow"]:contains("Charge Updated")');

                                if (charges.length > 1) {
                                    charges = charges.get().reverse();
                                }
                                /*
                                if (updates.length > 1) {
                                    updates = updates.get().reverse();
                                }                                
                                */

                                window.the_charges = new Array();
                                jQuery.each(charges, function(index, d) {
                                    var charge = jQuery('td', d);
                                    var temp_charges = charge[2].innerHTML.replace(/charge filed/ig, '').split('<br>');
                                    var temp_charges1 = new Array();
                                    temp_charges.forEach(function(d) {
                                        if (d.trim().length > 0 && /*/charge.*\d{1,}:/i.test(d)*/ !/information/i.test(d) && !/gerstein/i.test(d)) {
                                            temp_charges1.push(d);
                                        }
                                    });

                                    temp_charges1.forEach(function(e) {
                                      if(/charge\s\#?(\d{1,2}):(.*)/ig.test(e)){
                                        var results = /charge\s\#?(\d{1,2}):(.*)/ig.exec(e);
                                        //if(typeof result[1] !== "undefined" ){
                                           //console.log(results[1] + ' ' + results[2]);
                                          window.the_charges[results[1] - 1] = results[2];
                                        } else {
                                          window.the_charges.push(e.trim().replace(/^[-|:]/,'').trim());
                                        }
                                        //}
                                    });

                                });

                                /* Case updates 
                                window.the_updates = new Array();
                                jQuery.each(updates, function(index, d) {
                                    var update = jQuery('td', d);
                                    var temp_updates = update[2].innerHTML.replace(/charge updated/ig, '').split('<br>');
                                    var temp_updates1 = new Array();
                                    temp_updates.forEach(function(d) {
                                        if (d.trim().length > 0 &&  !/information/i.test(d) && !/gerstein/i.test(d)) {
                                            temp_updates1.push(d);
                                        }
                                    });

                                    temp_updates1.forEach(function(e) {
                                      if(/charge\s\#?(\d{1,2}):(.*)/ig.test(e)){
                                        var results1 = /charge\s\#?(\d{1,2}):(.*)/ig.exec(e);
                                        //if(typeof result[1] !== "undefined" ){
                                           //console.log(results[1] + ' ' + results[2]);
                                          window.the_charges[results1[1] - 1] = results1[2];
                                        } 
                                    });

                                });                                 
                                */  


                                // if first pass didn't work, use less precise extraction  
                                /*
                                if(!(window.the_charges.length > 0) ){
                                  jQuery.each(charges, function(index, d) {
                                    var charge = jQuery('td', d);
                                    var temp_charges = charge[2].innerHTML.replace(/charge filed/ig, '').split('<br>');
                                    var temp_charges1 = new Array();
                                    temp_charges.forEach(function(d) {
                                        if (d.trim().length > 0 ) {
                                            window.the_charges.push(d.replace(/^-/,'').trim());
                                        }
                                    });
                                */
                                    /*
                                    temp_charges1.forEach(function(e) {
                                        var results = /charge\s\#?(\d{1,2}):(.*)/ig.exec(e);
                                        //if(typeof result[1] !== "undefined" ){
                                           //console.log(results[1] + ' ' + results[2]);
                                          window.the_charges[results[1] - 1] = results[2];
                                        //}
                                    });
                                    */
                                //});

                                //}

                                //window.tds = this.document.querySelectorAll('tbody[id="appData:detailsform:jspdetailspage:docketInfo:DocketsInfo:tbody_element"] > tr > td');
                                //window.charges = tds[tds.length-1].innerHTML.replace('Charge Filed', '');
                                //console.log(window.number + ' ' + window.date );

                            });

                            this.then(function() {
                                var args = {};
                                var charges = this.getGlobal('the_charges');
                                args.type = the_type;
                                args.person = this.getGlobal('person');
                                args.number = this.getGlobal('number');
                                //args.count = i;
                                args.date = this.getGlobal('date');
                                /*
                                charge_list.forEach(function(d,j){
                                  var index = j + 1;
                                  args['charge_' + index] = d;

                                });
                                */

                                for (var k = 0; k < 35; k++) {
                                    var index = k + 1;
                                    var value = '';
                                    if (typeof charges[k] !== 'undefined' && charges[k] != null) {
                                        value = charges[k];
                                    }
                                    args['charge_' + index] = value.trim();

                                }
                                //args.charges = this.getGlobal('charges');
                                this.emit('csv', args);
                                this.emit('log_info', i );
                                //this.emit('spreadsheet', args);
                                //this.capture(this.getGlobal('number') + '.pdf');  

                            });
                        },function(){this.emit('log_error', i)});
                    });
                } else {
                  this.emit( 'log_warn', i );
                }
            });
            this.then(function() {
                if (this.exists('div.searchtoggleon')) {
                    this.thenClick('div.searchtoggleon');
                }
            });

            this.then(function() {
                i++;
            });
        }); // first repeat?
        // this.then(function(){ type_index++;});
        // }); // second repeat?

    }]);

    spooky.run();

    spooky.on('log_info', function(args) {
        logger.info(global.year + ' ' + global.the_type + ' ' + args + ' written');//info(line);//log.info(global.year + ' ' + global.the_type + ' ' + i + ' written to csv');
    
    });
    spooky.on('log_warn', function(args) {
        logger.warn(global.year + ' ' + global.the_type + ' ' + args + ' not found');
    
    });
    spooky.on('log_error', function(args) {
        logger.error(global.year + ' ' + global.the_type + ' ' + args + ' unavailable');
    
    });

    spooky.on('csv', function(line) {

        writer.write(line);

    });

    spooky.on('console', function(line) {
        console.log(line);
        //winston.log(line);
    });
    spooky.on('spreadsheet', function(thing) {

        var re = /(\d{4})\s(\w{3})\s/;
        var results = re.exec(thing.number);

        Spreadsheet.load({
            debug: true,
            spreadsheetId: '1Pr2fbvIsD8DKmKbr1ACPGZnNpZu8U1VxCaHj8HOvWcI',
            worksheetName: results[2].trim(),
            // Choose from 1 of the 3 authentication methods:
            //    1. Username and Password
            username: 'sexpanic@gmail.com',
            password: 'Th3reisamowerdeathyclept',
            // OR 2. OAuth
            //oauth : {
            //  email: '784051252425-cm421rtj2r8ck7cf46c1hfbgii6o5nfq@developer.gserviceaccount.com',
            //  keyFile: 'API Project-9f0a39f9ec49.json'
            //},
            // OR 3. Token
            // accessToken : {
            //   type: 'Bearer',
            //   token: 'AIzaSyB-iZcEXvzPTiNCmIzoruDZsqrgSSXiOJM'
            //}
        }, function sheetReady(err, spreadsheet) {

            var i = parseInt(thing.count);
            //var j = thing.number;
            //console.log(thing.count + ' ' + thing.number);
            var args = {};
            var charge_list = thing.charges.split('<br>');
            var charge_list1 = new Array();
            charge_list.forEach(function(d, j) {
                if (d.trim().length > 0) {
                    charge_list1.push(d.trim().replace(/charge.*\d:/gi, ''));
                }
            });

            args[i] = {};
            args[i][1] = thing.number.trim();
            args[i][3] = thing.person.trim();
            args[i][2] = thing.date.trim();
            charge_list1.forEach(function(d, j) {
                args[i][4 + j] = d.trim();
            });
            spreadsheet.add(args);
            //setTimeout(function(){
            spreadsheet.send(function(err) {
                if (err) {
                    console.log(err.name + ' ' + err.message); /*throw err;*/
                }
                console.log(i + " " + thing.number.trim() + " written");
            });
            //}, 2000);

        });

        //this.capture(number + '.pdf')
    });

    spooky.on('error', function(line) {
        writer.end();
        console.log("There was a problem.");
    });

    spooky.on('run.complete', function(args){
      writer.end();
      spooky.destroy();
      /*
      nc.notify({
        'title': global.year + ' ' + global.the_type,
        'subtitle': 'Finished',
        'message': global.year + '_' + global.the_type + '.csv',
        'sound': 'Purr',
        'wait': false 
      }, function(err, data) {
            //console.log(err);
            //  console.log(data);
      });
*/
    });

    spooky.on('exit', function(){
        global.nc.notify({
          'title': global.year + ' ' + global.the_type,
          'subtitle': 'Finished',
          'message': '',
          'sound': 'Funk', // case sensitive
          'appIcon': __dirname + '/coulson.jpg',
          'contentImage': __dirname + '/coulson.jpg',
          'open': 'file://' + __dirname + '/coulson.jpg'
        });
        writer.end();
    });

});
          
