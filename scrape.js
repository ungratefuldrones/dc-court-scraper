// https://www.npmjs.com/package/edit-google-spreadsheet
// https://github.com/WaterfallEngineering/SpookyJS
// https://www.dccourts.gov/cco/maincase.jsf
//var Spreadsheet = require('edit-google-spreadsheet');


// look at skipping again to 1) log skipped case info 2) retry 

var casper = require('casper').create({
	 clientScripts: ["bower_components/jquery/dist/jquery.min.js"]
	,logLevel :"debug"
	,verbose : true
});

casper.start('https://www.dccourts.gov/cco/maincase.jsf');

casper.page.onConsoleMessage = function (msg) {
  console.log(msg);
};

// fill out form
// click search
// click details
// scrape
// click 'view search info'
// goto 1

var i = 1;
casper.repeat(10, function(){    
	
	this.waitUntilVisible('form[name="appData:searchform"]', function(){
		
		// fill out form
		this.fill('form[name="appData:searchform"]', {
		    'appData:searchform:jspsearchpage:j_id_id14pc3':    "2015",
		    'appData:searchform:jspsearchpage:j_id_id18pc3':    i.toString(),
			'appData:searchform:jspsearchpage:selectCaseType' : "CMD",
		}, false);
	});

	// click search
	this.thenClick('input[name="appData:searchform:jspsearchpage:submitSearch"]');
	
	// wait until results visible
	this.waitUntilVisible('form[name="appData:searchform"]', function(){ 
		this.thenClick('input[name="appData:resultsform:jspresultspage:dt1:j_id_id41pc5"]');
	    this.then(function(){
	        this.wait(10000, function(){
	        	this.capture( i.toString() + '.png');   
	        });
	    });    
	});

	this.thenClick('div.searchtoggleon');
	casper.then(function(){ i++;});
});  

casper.run();